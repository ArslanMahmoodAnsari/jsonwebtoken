var {User} = require('./../models/user');

var authenticate = async(req, res, next) => {
  try {
    let token = req.header('x-auth');
    let user = await User.findByToken(token) 
    if (!user) {
      throw new Error('User not found')
    }

    req.user = user;
    req.token = token;
    next();    
  } catch (e) {
    res.status(401).send();
  } 
};

module.exports = {authenticate};

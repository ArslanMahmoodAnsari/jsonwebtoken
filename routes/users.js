const express = require("Express");
const router = express.Router();
const _ = require('lodash');
const {User} = require("../models/user");//for make the user object
let {authenticate} = require('./../middleware/authMiddleware');

//Create new user - signUp/Register
router.post('/add', async (req, res) => {
  try {
    const body = _.pick(req.body, ['email', 'password']);
    const user = new User(body);
    await user.save();
    let token = await user.generateAuthToken(); 
    res.header('x-auth', token).send(user);
   } catch (e) {
    res.status(400).send(e);
   }
})

// login the existing user - signIn
router.post('/login', async (req, res) => {
  try {
    const body = _.pick(req.body, ['email', 'password']); 
    const user = await User.findByCredentials(body.email, body.password);
    const token = await user.generateAuthToken();
    res.header('x-auth', token).send(user);
    } catch (e) {
      res.status(400).send(e.message);
   }

});

//delete a token form the user model - signOut 
router.delete('/logout', authenticate, async (req, res) => {
  try {
    await req.user.removeToken(req.token);  
    res.status(200).send();
  } catch (e) {
    res.status(400).send(e);
  }

});

//for checking purposes
router.get('/me',authenticate,(req,res)=>{
    res.send(req.user);
})



module.exports = router;
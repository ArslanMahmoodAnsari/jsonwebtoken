const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

let UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email'
    }
  },
  password: {
    type: String,
    require: true,
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
});

//An instance Method - for returning only the globle data not the secure data
UserSchema.methods.toJSON = function () {
    let user = this;
    let userObject = user.toObject();
    return _.pick(userObject, ['_id', 'email']);
  };

//An instance Method - for hashing/Salting the password 
UserSchema.methods.generateAuthToken = async function () {
  try {
    let user = this;
    let access = 'auth';
    let token = jwt.sign({_id: user._id.toHexString(), access}, process.env.JWT_SECRET).toString();
    user.tokens.push({access, token});
    await user.save();
    return token;
    } catch (e) {
     throw new Error(e); 
    }  
};

//An instance Method - for romving a token at the time of logging out
UserSchema.methods.removeToken = async function (token) {
  let user = this;
  return user.updateOne({
    $pull: {
      tokens: {token}
    }
  });
};

//An Model Method i.e like "findOneAndUpdate()" but UserDefined
UserSchema.statics.findByToken = async function (token) {
  try {
    let decoded = jwt.verify(token, process.env.JWT_SECRET);
    let user = User.findOne({
      '_id': decoded._id,
      'tokens.token': token,
      'tokens.access': 'auth'
    });
    return user;
  } catch (e) {
    return Promise.reject(e);
  }
};

//An Model Method -  for logging in the user
UserSchema.statics.findByCredentials = async function (email, password) {
  try {
    let user = await User.findOne({email}) ;
    if (!user) {
      throw new Error(`${email} not found`);
    }
    if(bcrypt.compareSync(password, user.password)){
      return user;
    }
    else{
      throw new Error('Password does not match');
    }
  } catch (e) {
    throw new Error(e);
  }

};

//a middleware function that is called before the "save()" executes because of ".pre()"
UserSchema.pre('save', function (next) {   
    var user = this;
    if (user.isModified('password')) {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(user.password, salt, (err, hash) => {
          user.password = hash;
          next();//so that the execution does not stop here or the application will crash, this return the control back to the calling funtion 
        });
      });
    }
    else {
      next();
    }
});

let User = mongoose.model('User', UserSchema);

module.exports = {User}